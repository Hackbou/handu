from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.


def home_app(request):
    name = "About Django"
    return render(request, 'pages/home.html', {'name': name})