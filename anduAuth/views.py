from django.shortcuts import HttpResponseRedirect, redirect, render
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout

# Create your views here.


"""
* la function qui me premet de s'inscrire
"""


def signUp(request):

    if request.method == 'POST':
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        confirm_password = request.POST['confirm_password']

        if confirm_password != password:
            print("les mot de passe ne correspondent pas")

        user = User.objects.create_user(username, email, password)

        if user is not None:
            print("User created")
            # user.save()
            return redirect('signIn')
        else:
            print("une erreur est survenue")

    return render(request, 'pages/signUp.html')


"""
* la function qui me premet de me connecter
"""


def signIn(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if user is not None:
            login(request, user)
            print("connected")

            # Redirect to a success page.
            return HttpResponseRedirect("/andApp/home/")
        else:
            print("donnees invalid")

    return render(request, 'pages/signIn.html')


def logout_view(request):
    logout(request)
