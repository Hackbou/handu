from django.apps import AppConfig


class AnduauthConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'anduAuth'
